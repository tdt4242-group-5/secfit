async function fetchUsers(search) {
	let response = await sendRequest("GET", `${HOST}/api/users?search=${search}`);

	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		let data = await response.json();

		let users = data.results;

		let container = document.getElementById("div-content");
		container.innerHTML = "";

		let currentUser = await getCurrentUser();

		if (!users) {
			container.innerHTML = '<p class="text-center">Could not fetch users</p>';
		} else if (users.length === 0) {
			container.innerHTML = `<p class="text-center">No users matching the search <i>${search}</i></p>`;
		} else {
			users.forEach((user) => {
				let templateUser = document.querySelector("#template-user");
				let cloneUser = templateUser.content.cloneNode(true);

				let aUser = cloneUser.querySelector("a");
				aUser.href = `profile.html?username=${user.username}`;
				let h5 = aUser.querySelector("h5");
				h5.textContent = user.username;
				if (currentUser && currentUser.username === user.username) {
					h5.innerHTML += ' <span class="badge bg-success">you</span>';
				}

				container.appendChild(aUser);
			});
		}
	}
}

window.addEventListener("DOMContentLoaded", async () => {
	let form = document.querySelector("#form-search");

	// Initial fetch all users
	await fetchUsers("");

	form.onsubmit = async (e) => {
		e.preventDefault();
		let search = document.querySelector("#input-username").value;
		await fetchUsers(search);
	};
});
