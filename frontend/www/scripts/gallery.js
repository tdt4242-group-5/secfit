let goBackButton;

const acceptedFileTypes = ["jpg", "png", "gif", "jpeg", "JPG", "PNG", "GIF", "JPEG"];

async function retrieveWorkoutImages(id) {
	let workoutData = null;
	const response = await sendRequest("GET", `${HOST}/api/workouts/${id}/`);
	if (!response.ok) {
		const data = await response.json();
		const alert = createAlert("Could not retrieve workout data!", data);
		document.body.prepend(alert);
	} else {
		workoutData = await response.json();

		document.getElementById("workout-title").innerHTML = "Workout name: " + workoutData["name"];
		document.getElementById("workout-owner").innerHTML =
			"Owner: " + workoutData["owner_username"];

		const hasNoImages = workoutData.files.length == 0;
		const noImageText = document.querySelector("#no-images-text");

		if (hasNoImages) {
			noImageText.classList.remove("hide");
			return;
		}

		noImageText.classList.add("hide");

		const filesDiv = document.getElementById("img-collection");
		const filesDeleteDiv = document.getElementById("img-collection-delete");

		const currentImageFileElement = document.querySelector("#current");
		let isFirstImg = true;

		let fileCounter = 0;

		for (const file of workoutData.files) {
			const fileType = file.file.split("/").at(-1).split(".")[1];

			const isImage = acceptedFileTypes.includes(fileType);

			if (isImage) {
				filesDiv.appendChild(createThumbnailImage(file));
				filesDeleteDiv.appendChild(createThumbnailDeleteButton(file, fileCounter));

				if (isFirstImg) {
					currentImageFileElement.src = file.file;
					isFirstImg = false;
				}

				fileCounter++;
			}
		}

		const otherImageFileElements = document.querySelectorAll(".imgs img");
		const selectedOpacity = 0.6;
		otherImageFileElements[0].style.opacity = selectedOpacity;

		otherImageFileElements.forEach((imageFileElement) => {
			imageFileElement.addEventListener("click", (event) =>
				handleThumbnailClick(event, currentImageFileElement, otherImageFileElements)
			);
		});
	}
}

function createThumbnailImage(file) {
	const img = document.createElement("img");
	img.src = file.file;

	return img;
}

function createThumbnailDeleteButton(file, fileCounter) {
	const deleteImgButton = document.createElement("input");
	deleteImgButton.type = "button";
	deleteImgButton.className = "btn btn-close";
	deleteImgButton.id = file.url.split("/")[file.url.split("/").length - 2];
	deleteImgButton.addEventListener("click", () => handleDeleteImgClick(deleteImgButton.id));

	deleteImgButton.style.left = `${(fileCounter % 4) * 191}px`;
	deleteImgButton.style.top = `${Math.floor(fileCounter / 4) * 105}px`;

	return deleteImgButton;
}

function handleThumbnailClick(event, currentImageFileElement, otherImageFileElements) {
	//Changes the main image
	currentImageFileElement.src = event.target.src;

	//Adds the fade animation
	currentImageFileElement.classList.add("fade-in");
	setTimeout(() => currentImageFileElement.classList.remove("fade-in"), 500);

	//Sets the opacity of the selected image to 0.4
	otherImageFileElements.forEach((imageFileElement) => (imageFileElement.style.opacity = 1));
	event.target.style.opacity = selectedOpacity;
}

async function validateImgFileType(id) {
	const file = await sendRequest("GET", `${HOST}/api/workout-files/${id}/`);
	const fileData = await file.json();
	const fileType = fileData.file.split("/")[fileData.file.split("/").length - 1].split(".")[1];

	return acceptedFileTypes.includes(fileType);
}

async function handleDeleteImgClick(id) {
	if (validateImgFileType(id)) {
		return;
	}

	const response = await sendRequest("DELETE", `${HOST}/api/workout-files/${id}/`);

	if (!response.ok) {
		const data = await response.json();
		const alert = createAlert(`Could not delete workout ${deleteImgButton.id}!`, data);
		document.body.prepend(alert);
	} else {
		location.reload();
	}
}

function handleGoBackToWorkoutClick(id) {
	window.location.replace(`workout.html?id=${id}`);
}

window.addEventListener("DOMContentLoaded", async () => {
	const urlParams = new URLSearchParams(window.location.search);
	const id = urlParams.get("id");

	goBackButton = document.querySelector("#btn-back-workout");
	goBackButton.addEventListener("click", () => handleGoBackToWorkoutClick(id));

	await retrieveWorkoutImages(id);
});
