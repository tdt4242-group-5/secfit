let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;
let muscleGroupSelector;

const formId = "#form-exercise";

const formFields = ["name", "description", "duration", "calories", "muscleGroup", "unit"];

const validMuscleGroupTypes = ["Legs", "Chest", "Back", "Arms", "Abdomen", "Shoulders"];

function validateMuscleGroup(formData) {
	const muscleGroup = formData.get("muscleGroup");
	const validMuscleGroup = validMuscleGroupTypes.includes(muscleGroup) ? muscleGroup : undefined;
	formData.set("muscleGroup", validMuscleGroup);
}

function disableForm() {
	setReadOnly(true, formId);
	muscleGroupSelector.setAttribute("disabled", "");
	okButton.className += " hide";
	deleteButton.className += " hide";
	cancelButton.className += " hide";
	editButton.className = editButton.className.replace(" hide", "");

	cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);
}

function handleCancelButtonDuringEdit() {
	disableForm();

	const form = document.querySelector(formId);

	formFields.forEach((field) => {
		if (oldFormData.has(field)) {
			form[field].value = oldFormData.get(field);
			oldFormData.delete(field);
		}
	});
}

function redirectToExercisesPage() {
	window.location.replace("exercises.html");
}

function createRequestBody() {
	const form = document.querySelector(formId);
	const formData = new FormData(form);

	validateMuscleGroup(formData);

	return formFields.reduce((acc, field) => ({ ...acc, [field]: formData.get(field) }), {});
}

async function createExercise() {
	document.querySelector("select").removeAttribute("disabled");

	const response = await sendRequest("POST", `${HOST}/api/exercises/`, createRequestBody());

	if (response.ok) {
		redirectToExercisesPage();
	} else {
		const data = await response.json();
		const alert = createAlert("Could not create new exercise!", data);
		document.body.prepend(alert);
	}
}

function handleEditExerciseButtonClick() {
	setReadOnly(false, formId);

	document.querySelector("select").removeAttribute("disabled");
	editButton.className += " hide";
	okButton.className = okButton.className.replace(" hide", "");
	cancelButton.className = cancelButton.className.replace(" hide", "");
	deleteButton.className = deleteButton.className.replace(" hide", "");

	cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

	const form = document.querySelector(formId);
	oldFormData = new FormData(form);
}

async function deleteExercise(id) {
	const response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
	if (!response.ok) {
		const data = await response.json();
		const alert = createAlert(`Could not delete exercise ${id}`, data);
		document.body.prepend(alert);
	} else {
		redirectToExercisesPage();
	}
}

async function retrieveExercise(id) {
	const response = await sendRequest("GET", `${HOST}/api/exercises/${id}/`);
	if (!response.ok) {
		const data = await response.json();
		const alert = createAlert("Could not retrieve exercise data!", data);
		document.body.prepend(alert);
	} else {
		document.querySelector("select").removeAttribute("disabled");
		const exerciseData = await response.json();
		const form = document.querySelector(formId);
		const formData = new FormData(form);

		for (const key of formData.keys()) {
			const selector =
				key !== "muscleGroup"
					? `input[name="${key}"], textarea[name="${key}"]`
					: `select[name=${key}]`;
			const input = form.querySelector(selector);
			const newVal = exerciseData[key];
			input.value = newVal;
		}
		document.querySelector("select").setAttribute("disabled", "");

		const elm = document.getElementById("owner_username_profile_link");
		elm.innerText = exerciseData["owner_username"];
		elm.href = `profile.html?username=${exerciseData["owner_username"]}`;
	}
}

async function updateExercise(id) {
	muscleGroupSelector.removeAttribute("disabled");

	const response = await sendRequest("PUT", `${HOST}/api/exercises/${id}/`, createRequestBody());

	if (!response.ok) {
		const data = await response.json();
		const alert = createAlert(`Could not update exercise ${id}`, data);
		document.body.prepend(alert);
	} else {
		disableForm();

		formFields.forEach((field) => {
			oldFormData.delete(field);
		});
	}
}

window.addEventListener("DOMContentLoaded", async () => {
	cancelButton = document.querySelector("#btn-cancel-exercise");
	okButton = document.querySelector("#btn-ok-exercise");
	deleteButton = document.querySelector("#btn-delete-exercise");
	editButton = document.querySelector("#btn-edit-exercise");
	muscleGroupSelector = document.querySelector("select");
	oldFormData = null;

	const urlParams = new URLSearchParams(window.location.search);

	// view/edit
	if (urlParams.has("id")) {
		const exerciseId = urlParams.get("id");
		await retrieveExercise(exerciseId);
		document.getElementById("owner_tag").classList.remove("hide");

		editButton.addEventListener("click", handleEditExerciseButtonClick);
		deleteButton.addEventListener(
			"click",
			((id) => deleteExercise(id)).bind(undefined, exerciseId)
		);
		okButton.addEventListener(
			"click",
			((id) => updateExercise(id)).bind(undefined, exerciseId)
		);
	}
	//create
	else {
		setReadOnly(false, formId);

		editButton.className += " hide";
		okButton.className = okButton.className.replace(" hide", "");
		cancelButton.className = cancelButton.className.replace(" hide", "");

		okButton.addEventListener("click", () => createExercise());
		cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
	}
});
