// Calculate time since a given datetime
// Adapted from https://stackoverflow.com/a/3177838
function timeSince(date) {
	var seconds = Math.floor((new Date() - date) / 1000);
	var interval = seconds / 31536000;

	if (interval > 1) {
		return Math.floor(interval) + "y";
	}

	interval = seconds / 2592000;

	if (interval > 1) {
		return Math.floor(interval) + "m";
	}

	interval = seconds / 86400;

	if (interval > 1) {
		return Math.floor(interval) + "d";
	}

	interval = seconds / 3600;

	if (interval > 1) {
		return Math.floor(interval) + "h";
	}

	interval = seconds / 60;

	if (interval > 1) {
		return Math.floor(interval) + "m";
	}

	return Math.floor(seconds) + "s";
}

async function fetchNotifications() {
	let response = await sendRequest("GET", `${HOST}/api/notifications/`);

	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		let data = await response.json();

		let notifications = data.results;

		let container = document.getElementById("notifications-container");
		container.innerHTML = "";

		if (!notifications) {
			container.innerHTML = '<p class="text-center">Could not fetch notifications</p>';
		} else if (notifications.length === 0) {
			container.innerHTML = `<p class="text-center">You have no notifications</p>`;
		} else {
			notifications.forEach((notification) => {
				let templateNotification = document.querySelector("#template-notification");
				let cloneNotification = templateNotification.content.cloneNode(true);

				let aNotification = cloneNotification.querySelector("a");
				aNotification.href = `${notification.content_type}.html?id=${notification.object_id}`;

				if (!notification.read) {
					aNotification.classList.add("list-group-item-primary");
				}

				let user = aNotification.querySelector("#notification-user");
				user.textContent = notification.content_object.owner_username;

				let contentType = aNotification.querySelector("#notification-content-type");
				contentType.textContent = notification.content_type;

				let contentName = aNotification.querySelector("#notification-content-name");
				contentName.textContent = notification.content_object.name;

				let time = aNotification.querySelector("#notification-time-since");
				time.textContent = timeSince(Date.parse(notification.timestamp.split(".")[0]));

				container.appendChild(aNotification);
			});
		}
	}
}

async function markAsRead() {
	await sendRequest("PUT", `${HOST}/api/notifications/read`);
}

window.addEventListener("DOMContentLoaded", async () => {
	await fetchNotifications();
	await markAsRead();
});
