async function fetchUserWorkouts(username) {
	let userWorkouts = null;
	let response = await sendRequest("GET", `${HOST}/api/users/${username}/workouts`);
	if (!response.ok) {
		console.log("COULD NOT RETRIEVE USER WORKOUTS");
	} else {
		let data = await response.json();
		userWorkouts = data;
	}
	return userWorkouts;
}

async function fetchUserExercises(username) {
	let userExercises = null;
	let response = await sendRequest("GET", `${HOST}/api/users/${username}/exercises`);
	if (!response.ok) {
		console.log("COULD NOT RETRIEVE USER EXERCISES");
	} else {
		let data = await response.json();
		userExercises = data;
	}
	return userExercises;
}

async function fetchUserMeals(username) {
	let userMeals = null;
	let response = await sendRequest("GET", `${HOST}/api/users/${username}/meals`);
	if (!response.ok) {
		console.log("COULD NOT RETRIEVE USER MEALS");
	} else {
		let data = await response.json();
		userMeals = data;
	}
	return userMeals;
}

async function getUser(username) {
	let response = await sendRequest("GET", `${HOST}/api/users/${username}`);
	if (!response.ok) {
		console.log("COULD NOT RETRIEVE USER");
	} else {
		let data = await response.json();
		user = data;
	}
	return user;
}

async function displayUser(username) {
	let user = await getUser(username);
	let current_user = await getCurrentUser();

	if (current_user && user.id != current_user.id) {
		displayButtonsContainer();

		if (user.subscribers.includes(current_user.username)) {
			displayUnsubscribeButton();
		} else {
			displaySubscribeButton();
		}
	}

	// Display profile data
	document.getElementById("username").innerText = user.username;
	document.getElementById("subscriptions").innerText = user.subscriptions_count;
	document.getElementById("subscribers").innerText = user.subscribers_count;

	// Display profile content
	let userWorkouts = await fetchUserWorkouts(username);
	let container = document.getElementById("workout-content");
	userWorkouts.results.forEach((workout) => {
		let templateWorkout = document.querySelector("#template-workout");
		let cloneWorkout = templateWorkout.content.cloneNode(true);

		let aWorkout = cloneWorkout.querySelector("a");
		aWorkout.href = `workout.html?id=${workout.id}`;

		let h5 = aWorkout.querySelector("h5");
		h5.textContent = workout.name;

		let localDate = new Date(workout.date);

		let table = aWorkout.querySelector("table");
		let rows = table.querySelectorAll("tr");
		rows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
		rows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time
		rows[2].querySelectorAll("td")[1].textContent = workout.exercise_instances.length; // Exercises

		container.appendChild(aWorkout);
	});

	let userExercises = await fetchUserExercises(username);
	container = document.getElementById("exercise-content");
	let exerciseTemplate = document.querySelector("#template-exercise");
	userExercises.results.forEach((exercise) => {
		const exerciseAnchor = exerciseTemplate.content.firstElementChild.cloneNode(true);
		exerciseAnchor.href = `exercise.html?id=${exercise.id}`;

		const h5 = exerciseAnchor.querySelector("h5");
		h5.textContent = exercise.name;

		const p = exerciseAnchor.querySelector("p");
		p.textContent = exercise.description;

		container.appendChild(exerciseAnchor);
	});

	let userMeals = await fetchUserMeals(username);
	container = document.getElementById("meal-content");
	userMeals.results.forEach((meal) => {
		let templateMeal = document.querySelector("#template-meal");
		let cloneMeal = templateMeal.content.cloneNode(true);

		let aMeal = cloneMeal.querySelector("a");
		aMeal.href = `meal.html?id=${meal.id}`;

		let h5 = aMeal.querySelector("h5");
		h5.textContent = meal.name;

		let localDate = new Date(meal.date);

		let table = aMeal.querySelector("table");
		let rows = table.querySelectorAll("tr");
		rows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
		rows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time

		container.appendChild(aMeal);
	});
}

function displayButtonsContainer() {
	document.getElementById("button-container").style.display = "block";
}

function displaySubscribeButton() {
	document.getElementById("button-user-subscribe").classList.remove("hide");
}

function displayUnsubscribeButton() {
	document.getElementById("button-user-unsubscribe").classList.remove("hide");
}

async function subscribeToUser(username) {
	let response = await sendRequest("POST", `${HOST}/api/users/${username}/subscribers`);

	if (response.ok) {
		window.location.reload();
	}
}

async function unsubscribeToUser(username) {
	let response = await sendRequest("DELETE", `${HOST}/api/users/${username}/subscribers`);

	if (response.ok) {
		window.location.reload();
	}
}

window.addEventListener("DOMContentLoaded", async () => {
	const urlParams = new URLSearchParams(window.location.search);
	const username = urlParams.get("username");

	displayUser(username);

	const subscribeButton = document.getElementById("button-user-subscribe");
	subscribeButton.addEventListener("click", () => subscribeToUser(username));

	const unsubscribeButton = document.getElementById("button-user-unsubscribe");
	unsubscribeButton.addEventListener("click", () => unsubscribeToUser(username));

	const workoutsContainer = document.getElementById("workout-content");
	const exercisesContainer = document.getElementById("exercise-content");
	const mealsContainer = document.getElementById("meal-content");

	const workoutsButton = document.getElementById("list-user-workouts");
	workoutsButton.addEventListener("click", () => {
		workoutsContainer.classList.remove("hide");
		exercisesContainer.classList.add("hide");
		mealsContainer.classList.add("hide");
	});
	const exercisesButton = document.getElementById("list-user-exercises");
	exercisesButton.addEventListener("click", () => {
		exercisesContainer.classList.remove("hide");
		workoutsContainer.classList.add("hide");
		mealsContainer.classList.add("hide");
	});
	const mealsButton = document.getElementById("list-user-meals");
	mealsButton.addEventListener("click", () => {
		mealsContainer.classList.remove("hide");
		exercisesContainer.classList.add("hide");
		workoutsContainer.classList.add("hide");
	});
});
