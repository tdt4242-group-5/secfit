/**
 * A visitor should:
 * - see all public workouts
 * - not see coach workouts
 * - not see private workouts
 *
 * Since the role "visitor" is not defined anywhere, it is assumed that it is a logged in user that is not a coach or an athlete.
 */

describe("visitor workout visibility", () => {
	// Visitor user credentials
	const username = "user3";
	const password = "user3";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	describe("displays public workout", () => {
		it("displays public workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user1 public");
		});

		it("displays public workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(7, "user1 public");
		});
	});

	describe("do not display coach workout", () => {
		it("do not display coach workout in workouts list", () => {
			cy.workoutShouldNotBeVisibleInWorkoutsList("user1 coach");
		});

		it("do not display coach workout on workout detail page", () => {
			cy.workoutShouldNotBeVisibleOnWorkoutDetailPage(6, "user1 coach");
		});
	});

	describe("do not display private workout", () => {
		it("do not display private workout in workouts list", () => {
			cy.workoutShouldNotBeVisibleInWorkoutsList("user1 private");
		});

		it("do not display private workout on workout detail page", () => {
			cy.workoutShouldNotBeVisibleOnWorkoutDetailPage(5, "user1 private");
		});
	});
});
