/**
 * An athlete should:
 * - see their own private workouts
 * - see their own coach workouts
 * - see their own public workouts
 * - see others public workouts
 * - not see others coach workouts
 * - not see others private workouts
 */

describe("athlete workout visibility", () => {
	// Athlete user credentials
	const username = "user2";
	const password = "user2";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	describe("displays own private workout", () => {
		it("displays own private workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user2 private");
		});

		it("displays own private workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(3, "user2 private");
		});
	});

	describe("displays own coach workout", () => {
		it("displays own coach workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user2 coach");
		});

		it("displays own coach workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(2, "user2 coach");
		});
	});

	describe("displays own public workout", () => {
		it("displays own public workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user2 public");
		});

		it("displays own coach workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(1, "user2 public");
		});
	});

	describe("displays others public workout", () => {
		it("displays others public workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user1 public");
		});

		it("displays others public workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(7, "user1 public");
		});
	});

	describe("do not display others coach workout", () => {
		it("do not display others coach workout in workouts list", () => {
			cy.workoutShouldNotBeVisibleInWorkoutsList("user1 coach");
		});

		it("do not display others coach workout on workout detail page", () => {
			cy.workoutShouldNotBeVisibleOnWorkoutDetailPage(6, "user1 coach");
		});
	});

	describe("do not display others private workout", () => {
		it("do not display others private workout in workouts list", () => {
			cy.workoutShouldNotBeVisibleInWorkoutsList("user1 private");
		});

		it("do not display others private workout on workout detail page", () => {
			cy.workoutShouldNotBeVisibleOnWorkoutDetailPage(5, "user1 private");
		});
	});
});
