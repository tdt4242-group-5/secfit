/**
 * A coach should:
 * - see their own private workouts
 * - see their own coach workouts
 * - see their own public workouts
 * - see others public workouts
 * - see their athletes coach workouts
 * - not see other athletes (not their athletes) coach workouts
 * - not see others private workouts
 */

describe("coach workout visibility", () => {
	// Coach user credentials
	const username = "user1";
	const password = "user1";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	describe("displays own private workout", () => {
		it("displays own private workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user1 private");
		});

		it("displays own private workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(5, "user1 private");
		});
	});

	describe("displays own coach workout", () => {
		it("displays own coach workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user1 coach");
		});

		it("displays own coach workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(6, "user1 coach");
		});
	});

	describe("displays own public workout", () => {
		it("displays own public workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user1 public");
		});

		it("displays own coach workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(7, "user1 public");
		});
	});

	describe("displays others public workout", () => {
		it("displays others public workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user2 public");
		});

		it("displays others public workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(1, "user2 public");
		});
	});

	describe("displays athletes coach workout", () => {
		it("displays athletes coach workout in workouts list", () => {
			cy.workoutShouldBeVisibleInWorkoutsList("user2 coach");
		});

		it("displays athletes coach workout on workout detail page", () => {
			cy.workoutShouldBeVisibleOnWorkoutDetailPage(2, "user2 coach");
		});
	});

	describe("do not display other athletes (not their athletes) coach workout", () => {
		it("do not display other athletes (not their athletes) coach workout in workouts list", () => {
			cy.workoutShouldNotBeVisibleInWorkoutsList("user3 coach");
		});

		it("do not display other athletes (not their athletes) coach workout on workout detail page", () => {
			cy.workoutShouldNotBeVisibleOnWorkoutDetailPage(4, "user3 coach");
		});
	});

	describe("do not display others private workout", () => {
		it("do not display others private workout in workouts list", () => {
			cy.workoutShouldNotBeVisibleInWorkoutsList("user2 private");
		});

		it("do not display others private workout on workout detail page", () => {
			cy.workoutShouldNotBeVisibleOnWorkoutDetailPage(3, "user2 private");
		});
	});
});
