describe("user search", () => {
	// User credentials
	const username = "user2";
	const password = "user2";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	it("page loads", () => {
		cy.visit("index.html");
		cy.get(".navbar-nav").contains("User search").click();

		cy.location("pathname").should("eq", "/usersearch.html");

		cy.get("#input-username").should("be.visible");
		cy.get("#button-user-search").should("be.visible");

		cy.get(".list-group").find("a").should("have.length", 6);
		cy.get(".list-group").contains("user2").get(".badge").should("be.visible");
	});

	it("no matching user", () => {
		cy.visit("usersearch.html");

		cy.get("#input-username").type("nonexistent");
		cy.get("#button-user-search").click();
		cy.contains("No users matching the search nonexistent").should("be.visible");
	});

	it("searching", () => {
		cy.visit("usersearch.html");

		cy.get("#input-username").type("user");
		cy.get("#button-user-search").click();

		cy.get(".list-group").find("a").should("have.length", 5);
		cy.get(".list-group").contains("user1").should("be.visible");
		cy.get(".list-group").contains("user2").should("be.visible");
		cy.get(".list-group").contains("user3").should("be.visible");
		cy.get(".list-group").contains("admin").should("not.exist");

		cy.get("#input-username").type("1");
		cy.get("#button-user-search").click();

		cy.get(".list-group").find("a").should("have.length", 1);
		cy.get(".list-group").contains("user1").should("be.visible");
		cy.get(".list-group").contains("user1").click();

		cy.location("pathname").should("eq", "/profile.html");
		cy.location("search").should("eq", "?username=user1");
	});
});
