describe("subscription", () => {
	// User credentials
	const username = "user2";
	const password = "user2";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	it("subscribe to user", () => {
		// Check user2's subscriptions count
		cy.visit(`/profile.html?username=${username}`);
		cy.get("#subscriptions").should("have.text", "0");

		// Check user1's subscribers count
		cy.visit("/profile.html?username=user1");
		cy.get("#subscribers").should("have.text", "0");

		// Subscribe to user1 and check increased subscribers count
		cy.get("#button-user-subscribe").click();
		cy.get("#button-user-subscribe").should("not.be.visible");
		cy.get("#button-user-unsubscribe").should("be.visible");
		cy.get("#subscribers").should("have.text", "1");

		// Check increased subscriptions count for user2
		cy.visit(`/profile.html?username=${username}`);
		cy.get("#subscriptions").should("have.text", "1");
	});

	it("unsubscribe to user", () => {
		// Check user2's subscriptions count
		cy.visit(`/profile.html?username=${username}`);
		cy.get("#subscriptions").should("have.text", "1");

		// Check user1's subscribers count
		cy.visit("/profile.html?username=user1");
		cy.get("#subscribers").should("have.text", "1");

		// Unubscribe to user1 and check decreased subscribers count
		cy.get("#button-user-unsubscribe").click();
		cy.get("#button-user-unsubscribe").should("not.be.visible");
		cy.get("#button-user-subscribe").should("be.visible");
		cy.get("#subscribers").should("have.text", "0");

		// Check decreased subscriptions count for user2
		cy.visit(`/profile.html?username=${username}`);
		cy.get("#subscriptions").should("have.text", "0");
	});
});
