describe("visit profile through author", () => {
	// User credentials
	const username = "user2";
	const password = "user2";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	it("visit profile through author of workout", () => {
		cy.visit("workout.html?id=7");
		cy.get("#owner_username_profile_link").then((link) => {
			cy.wrap(link).should("be.visible");
			cy.wrap(link).should("have.text", "user1");
			cy.wrap(link).click();
		});

		cy.location("pathname").should("eq", "/profile.html");
		cy.location("search").should("eq", "?username=user1");
	});

	it("visit profile through author of exercise", () => {
		cy.visit("exercise.html?id=1");
		cy.get("#owner_username_profile_link").then((link) => {
			cy.wrap(link).should("be.visible");
			cy.wrap(link).should("have.text", "user2");
			cy.wrap(link).click();
		});

		cy.location("pathname").should("eq", "/profile.html");
		cy.location("search").should("eq", "?username=user2");
	});

	it("visit profile through author of meal", () => {
		cy.visit("meal.html?id=1");
		cy.get("#owner_username_profile_link").then((link) => {
			cy.wrap(link).should("be.visible");
			cy.wrap(link).should("have.text", "user1");
			cy.wrap(link).click();
		});

		cy.location("pathname").should("eq", "/profile.html");
		cy.location("search").should("eq", "?username=user1");
	});
});
