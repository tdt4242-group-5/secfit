describe("profile", () => {
	// User credentials
	const username = "user2";
	const password = "user2";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	it("page loads", () => {
		cy.visit("index.html");
		cy.get(".navbar-nav").contains(`Profile (${username})`).click();
		cy.location("pathname").should("eq", "/profile.html");
		cy.location("search").should("eq", `?username=${username}`);
	});

	it("own profile content", () => {
		cy.visit(`/profile.html?username=${username}`);

		cy.get("#username").contains(username).should("be.visible");
		cy.get("#button-user-subscribe").should("not.be.visible");
		cy.get("#button-user-unsubscribe").should("not.be.visible");

		// Test subsciber and subscriptions counts
		cy.get("#subscribers").should("be.visible");
		cy.get("#subscribers")
			.invoke("text")
			.should("match", /^[0-9]*$/);
		cy.get("#subscriptions").should("be.visible");
		cy.get("#subscriptions")
			.invoke("text")
			.should("match", /^[0-9]*$/);

		// Test default content visible
		cy.get("#workout-content").should("be.visible");
		cy.get("#exercise-content").should("not.be.visible");
		cy.get("#meal-content").should("not.be.visible");

		// Test workouts list
		cy.get("#list-tab").contains("Workouts").should("have.class", "active");
		cy.get("#list-tab").contains("Exercises").should("not.have.class", "active");
		cy.get("#list-tab").contains("Meals").should("not.have.class", "active");

		cy.get("#workout-content").find("a").should("have.length", 1);
		cy.get("#workout-content").contains("user2 public").click();

		cy.location("pathname").should("eq", "/workout.html");
		cy.location("search").should("eq", `?id=1`);

		cy.go("back");

		// Test exercises list
		cy.get("#list-tab").contains("Exercises").click();

		cy.get("#list-tab").contains("Workouts").should("not.have.class", "active");
		cy.get("#list-tab").contains("Exercises").should("have.class", "active");
		cy.get("#list-tab").contains("Meals").should("not.have.class", "active");

		cy.get("#exercise-content").find("a").should("have.length", 2);
		cy.get("#exercise-content").contains("Push-ups").click();

		cy.location("pathname").should("eq", "/exercise.html");
		cy.location("search").should("eq", `?id=1`);

		cy.go("back");

		// Test meals list
		cy.get("#list-tab").contains("Meals").click();

		cy.get("#list-tab").contains("Workouts").should("not.have.class", "active");
		cy.get("#list-tab").contains("Exercises").should("not.have.class", "active");
		cy.get("#list-tab").contains("Meals").should("have.class", "active");

		cy.get("#meal-content").find("a").should("have.length", 0);
	});

	it("others profile content", () => {
		cy.visit("/profile.html?username=user1");

		cy.get("#username").contains("user1").should("be.visible");
		cy.get("#button-user-subscribe").should("be.visible");
		cy.get("#button-user-unsubscribe").should("not.be.visible");

		// Test subsciber and subscriptions counts
		cy.get("#subscribers").should("be.visible");
		cy.get("#subscribers")
			.invoke("text")
			.should("match", /^[0-9]*$/);
		cy.get("#subscriptions").should("be.visible");
		cy.get("#subscriptions")
			.invoke("text")
			.should("match", /^[0-9]*$/);

		// Test default content visible
		cy.get("#workout-content").should("be.visible");
		cy.get("#exercise-content").should("not.be.visible");
		cy.get("#meal-content").should("not.be.visible");

		// Test workouts list
		cy.get("#list-tab").contains("Workouts").should("have.class", "active");
		cy.get("#list-tab").contains("Exercises").should("not.have.class", "active");
		cy.get("#list-tab").contains("Meals").should("not.have.class", "active");

		cy.get("#workout-content").find("a").should("have.length", 1);
		cy.get("#workout-content").contains("user1 public").click();

		cy.location("pathname").should("eq", "/workout.html");
		cy.location("search").should("eq", `?id=7`);

		cy.go("back");

		// Test exercises list
		cy.get("#list-tab").contains("Exercises").click();

		cy.get("#list-tab").contains("Workouts").should("not.have.class", "active");
		cy.get("#list-tab").contains("Exercises").should("have.class", "active");
		cy.get("#list-tab").contains("Meals").should("not.have.class", "active");

		cy.get("#exercise-content").find("a").should("have.length", 0);

		// Test meals list
		cy.get("#list-tab").contains("Meals").click();

		cy.get("#list-tab").contains("Workouts").should("not.have.class", "active");
		cy.get("#list-tab").contains("Exercises").should("not.have.class", "active");
		cy.get("#list-tab").contains("Meals").should("have.class", "active");

		cy.get("#meal-content").find("a").should("have.length", 1);
		cy.get("#meal-content").contains("user1 public").click();

		cy.location("pathname").should("eq", "/meal.html");
		cy.location("search").should("eq", `?id=1`);

		cy.go("back");
	});
});
