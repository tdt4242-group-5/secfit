describe("notifications", () => {
	// User credentials
	const username = "user4";
	const password = "user4";

	before(() => {
		cy.visit("index.html");
		cy.clearCookies();

		// Log in once before any tests run
		cy.login(username, password);
	});

	beforeEach(() => {
		// Before each test, preserve the access and refresh cookies (i.e. stay logged in)
		Cypress.Cookies.preserveOnce("access", "refresh");

		cy.window().then((win) => {
			win.sessionStorage.setItem("username", username);
		});
	});

	it("page loads", () => {
		cy.visit("index.html");
		cy.get(".navbar-nav").contains("Notifications").click();
		cy.location("pathname").should("eq", "/notifications.html");
	});

	it("receive notifications when user you subscribe to posts content", () => {
		// Check that no notifications are received
		cy.visit("notifications.html");
		cy.contains("You have no notifications").should("be.visible");

		/**
		 * user4 subscribes to user5, so user4 should receive notifications when user5
		 * posts workouts, exercises and meals
		 */

		// Create workout
		cy.login("user5", "user5");
		cy.createWorkout("user5 workout", new Date(), "notes", "PU", [
			{ exercise: "http://localhost:8000/api/exercises/1/", number: "10", sets: "10" }
		]);

		// Check if workout notification is received and points to correct workout
		cy.login("user4", "user4");
		cy.visit("index.html");
		cy.get("#nav-notifications-count").should("be.visible"); // Check not read
		cy.get("#nav-notifications-count").should("have.text", "1"); // Check not read

		cy.visit("notifications.html");
		cy.get("#notifications-container").find("a").should("have.length", 1);
		cy.get("#notifications-container")
			.contains("user5 posted a new workout: user5 workout")
			.should("be.visible");
		cy.get("#notifications-container")
			.contains("user5 posted a new workout: user5 workout")
			.should("have.class", "list-group-item-primary"); // Check not read

		cy.get("#notifications-container")
			.contains("user5 posted a new workout: user5 workout")
			.click();
		cy.get("#inputName").should("have.value", "user5 workout");

		cy.get("#nav-notifications-count").should("not.be.visible"); // Check read
		cy.visit("notifications.html");
		cy.get("#notifications-container")
			.contains("user5 posted a new workout: user5 workout")
			.should("not.have.class", "list-group-item-primary"); // Check read

		// Create exercise and meal
		cy.login("user5", "user5");
		cy.createExercise("user5 exercise", "desc", "reps", "10", "100", "Legs");
		cy.createMeal("user5 meal", new Date(), "PU", "notes", "100");

		// Check if exercise and meal notifications are received and point to correct exercise and meal
		cy.login("user4", "user4");
		cy.visit("index.html");
		cy.get("#nav-notifications-count").should("be.visible"); // Check not read
		cy.get("#nav-notifications-count").should("have.text", "2"); // Check not read

		cy.visit("notifications.html");
		cy.get("#notifications-container").find("a").should("have.length", 3);

		cy.get("#notifications-container")
			.contains("user5 posted a new exercise: user5 exercise")
			.should("be.visible");
		cy.get("#notifications-container")
			.contains("user5 posted a new meal: user5 meal")
			.should("be.visible");

		cy.get("#notifications-container")
			.contains("user5 posted a new exercise: user5 exercise")
			.should("have.class", "list-group-item-primary"); // Check not read
		cy.get("#notifications-container")
			.contains("user5 posted a new meal: user5 meal")
			.should("have.class", "list-group-item-primary"); // Check not read

		cy.get("#notifications-container")
			.contains("user5 posted a new exercise: user5 exercise")
			.click();
		cy.get("#inputName").should("have.value", "user5 exercise");

		cy.go("back");

		cy.get("#notifications-container").contains("user5 posted a new meal: user5 meal").click();
		cy.get("#inputName").should("have.value", "user5 meal");

		cy.get("#nav-notifications-count").should("not.be.visible"); // Check read
		cy.visit("notifications.html");
		cy.get("#notifications-container")
			.contains("user5 posted a new exercise: user5 exercise")
			.should("not.have.class", "list-group-item-primary"); // Check read
		cy.get("#notifications-container")
			.contains("user5 posted a new meal: user5 meal")
			.should("not.have.class", "list-group-item-primary"); // Check read
	});
});
