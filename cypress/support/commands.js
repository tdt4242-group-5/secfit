// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add("login", (username, password) => {
	cy.intercept({
		method: "GET",
		url: "http://localhost:8000/api/users/?user=current"
	}).as("getCurrentUser");

	cy.request("POST", "http://localhost:8000/api/token/", {
		username: username,
		password: password
	})
		.its("body")
		.then((res) => {
			cy.setCookie("access", res.access);
			cy.setCookie("refresh", res.refresh);
		});

	cy.visit("index.html");
	cy.wait("@getCurrentUser");
	cy.get("#btn-logout").should("be.visible");
});

Cypress.Commands.add("workoutShouldBeVisibleInWorkoutsList", (title) => {
	// Check if visible in workout list
	cy.intercept({
		method: "GET",
		url: "http://localhost:8000/api/workouts/?ordering=-date"
	}).as("getWorkouts");

	cy.visit("workouts.html");
	cy.wait("@getWorkouts");
	cy.contains(title).should("be.visible");
});

Cypress.Commands.add("workoutShouldBeVisibleOnWorkoutDetailPage", (id, title) => {
	// Check if visible on workout page
	cy.intercept({
		method: "GET",
		url: `http://localhost:8000/api/workouts/${id}/`
	}).as("getWorkout");

	cy.visit(`workout.html?id=${id}`);
	cy.wait("@getWorkout");
	cy.get("#inputName").should("have.value", title);
	cy.contains("Could not retrieve workout data!").should("not.exist");
});

Cypress.Commands.add("workoutShouldNotBeVisibleInWorkoutsList", (title) => {
	// Check if not visible in workout list
	cy.intercept({
		method: "GET",
		url: "http://localhost:8000/api/workouts/?ordering=-date"
	}).as("getWorkouts");

	cy.visit("workouts.html", { failOnStatusCode: false });
	cy.wait("@getWorkouts");
	cy.contains(title).should("not.exist");
});

Cypress.Commands.add("workoutShouldNotBeVisibleOnWorkoutDetailPage", (id, title) => {
	// Check if not visible on workout page
	cy.intercept({
		method: "GET",
		url: `http://localhost:8000/api/workouts/${id}/`
	}).as("getWorkout");

	cy.visit(`workout.html?id=${id}`, { failOnStatusCode: false });
	cy.wait("@getWorkout");
	cy.get("#inputName").should("not.have.value", title);
	cy.contains("Could not retrieve workout data!").should("be.visible");
});

Cypress.Commands.add("createWorkout", (name, date, notes, visibility, exerciseInstances) => {
	cy.getCookie("access").then((cookie) => {
		cy.request({
			method: "POST",
			url: "http://localhost:8000/api/workouts/",
			headers: {
				Authorization: `Bearer ${cookie.value}`
			},
			body: {
				name: name,
				date: date,
				notes: notes,
				visibility: visibility,
				exercise_instances: exerciseInstances
			}
		}).then((response) => {
			expect(response.status).to.eq(201);
		});
	});
});

Cypress.Commands.add(
	"createExercise",
	(name, description, unit, duration, calories, muscleGroup) => {
		cy.getCookie("access").then((cookie) => {
			cy.request({
				method: "POST",
				url: "http://localhost:8000/api/exercises/",
				headers: {
					Authorization: `Bearer ${cookie.value}`
				},
				body: {
					name: name,
					description: description,
					unit: unit,
					duration: duration,
					calories: calories,
					muscleGroup: muscleGroup
				}
			}).then((response) => {
				expect(response.status).to.eq(201);
			});
		});
	}
);

Cypress.Commands.add("createMeal", (name, date, visibility, notes, calories) => {
	cy.getCookie("access").then((cookie) => {
		cy.request({
			method: "POST",
			url: "http://localhost:8000/api/meals/",
			headers: {
				Authorization: `Bearer ${cookie.value}`
			},
			body: {
				name: name,
				date: date,
				visibility: visibility,
				notes: notes,
				calories: calories
			}
		}).then((response) => {
			expect(response.status).to.eq(201);
		});
	});
});
