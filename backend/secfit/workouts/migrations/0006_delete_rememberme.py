# Generated by Django 3.1 on 2022-04-03 11:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workouts', '0005_exercise_owner'),
    ]

    operations = [
        migrations.DeleteModel(
            name='RememberMe',
        ),
    ]
