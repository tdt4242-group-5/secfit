from rest_framework import mixins, generics
from workouts.mixins import CreateListModelMixin
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from users.serializers import (
    UserSerializer,
    OfferSerializer,
    AthleteFileSerializer,
    UserPutSerializer,
    UserGetSerializer,
    RememberMeSerializer
)
from rest_framework.permissions import (
    IsAuthenticatedOrReadOnly,
)
from users.models import Offer, AthleteFile
from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework.parsers import MultiPartParser, FormParser
from users.permissions import IsCurrentUser, IsAthlete, IsCoach
from workouts.permissions import IsOwner, IsReadOnly
from .models import User
from collections import namedtuple
import base64, pickle
from django.core.signing import Signer
from django.core.exceptions import PermissionDenied
from rest_framework_simplejwt.tokens import RefreshToken

# Create your views here.
class UserList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = UserSerializer
    users = []
    admins = []

    def get(self, request, *args, **kwargs):
        self.serializer_class = UserGetSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        query_set = get_user_model().objects.all()

        if self.request.user:
            query_parameters = self.request.query_params
            query_set = self.filter_queryset_by_user(query_set, query_parameters)
            query_set = self.filter_queryset_by_searchparam(query_set, query_parameters)
   
        return query_set      

    def filter_queryset_by_user(self, query_set, query_parameters):
        # Return the currently logged in user
        user_param = query_parameters.get("user", None)
        if user_param and user_param == "current":
            query_set = get_user_model().objects.filter(pk=self.request.user.pk)
        return query_set
    
    def filter_queryset_by_searchparam(self, query_set, query_parameters):
        # Return the users matching the search
        search_param = query_parameters.get("search", None)
        if search_param:
            query_set = get_user_model().objects.filter(username__icontains=search_param)
        return query_set
        

class UserDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    lookup_field_options = ["pk", "username"]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    permission_classes = [permissions.IsAuthenticated & (IsCurrentUser | IsReadOnly)]

    def get_object(self):
        for field in self.lookup_field_options:
            if field in self.kwargs:
                self.lookup_field = field
                break

        return super().get_object()

    def get(self, request, *args, **kwargs):
        self.serializer_class = UserGetSerializer
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        self.serializer_class = UserPutSerializer
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class UserSubscribers(APIView):
    # Subscribe to user
    def post(self, request, *args, username):
        if self.request.user and (username is not None):
            other = User.objects.get(username=username)
            if other:
                other.subscribers.add(self.request.user)

        return Response()
    
    # Unsubscribe to user
    def delete(self, request, *args, username):
        if self.request.user and (username is not None):
            other = User.objects.get(username=username)
            if other:
                other.subscribers.remove(self.request.user)
        
        return Response()


class OfferList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        result = Offer.objects.none()

        if self.request.user:
            query_set = Offer.objects.filter(
                Q(owner=self.request.user) | Q(recipient=self.request.user)
            ).distinct()
            query_parameters = self.request.query_params
            user = self.request.user
            query_set = self.filter_queryset_by_status(query_set, query_parameters, user)
            query_set = self.filter_queryset_by_category(query_set, query_parameters, user)
            return query_set
        else:
            return result

    def filter_queryset_by_status(self, query_set, query_parameters, user):
        status = query_parameters.get("status", None)
        if status is not None and self.request is not None:
            query_set = query_set.filter(status=status)
            if query_parameters.get("status", None) is None:
                return Offer.objects.filter(Q(owner=user)).distinct()
        return query_set

    def filter_queryset_by_category(self, query_set, query_parameters, user):
        category = query_parameters.get("category", None)
        if category is not None and query_parameters is not None:
            if category == "sent":
                query_set = query_set.filter(owner=user)
            elif category == "received":
                query_set = query_set.filter(recipient=user)
        return query_set

class OfferDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class AthleteFileList(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    CreateListModelMixin,
    generics.GenericAPIView,
):
    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsCoach)]
    parser_classes = [MultiPartParser, FormParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        query_set = AthleteFile.objects.none()

        if self.request.user:
            query_set = AthleteFile.objects.filter(
                Q(athlete=self.request.user) | Q(owner=self.request.user)
            ).distinct()

        return query_set


class AthleteFileDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsOwner)]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# Allow users to save a persistent session in their browser
class RememberMe(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):

    serializer_class = RememberMeSerializer

    def get(self, request):
        if request.user.is_authenticated == False:
            raise PermissionDenied
        else:
            return Response({"remember_me": self.rememberme()})

    def post(self, request):
        cookieObject = namedtuple("Cookies", request.COOKIES.keys())(
            *request.COOKIES.values()
        )
        user = self.get_user(cookieObject)
        refresh = RefreshToken.for_user(user)
        return Response(
            {
                "refresh": str(refresh),
                "access": str(refresh.access_token),
            }
        )

    def get_user(self, cookieObject):
        decode = base64.b64decode(cookieObject.remember_me)
        user, sign = pickle.loads(decode)

        # Validate signature
        if sign == self.sign_user(user):
            return user

    def rememberme(self):
        creds = [self.request.user, self.sign_user(str(self.request.user))]
        return base64.b64encode(pickle.dumps(creds))

    def sign_user(self, username):
        signer = Signer()
        signed_user = signer.sign(username)
        return signed_user
