"""
Boundary value tests for the register page.
"""

from tests.boundary_value.utils import ExtendedTestCase

from rest_framework.test import APIClient


class RegisterPageTest(ExtendedTestCase):

    def setUp(self):
        self.client = APIClient()
        self.api_url = "/api/users/"

        self.nom_object = {
            "username": "test123",
            "email": "test@test.com",
            "password": "password123",
            "password1": "password123",
            "phone_number": "12345678",
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Kongsgårdsgata 2"
        }

    def test_nom_object(self):
        response = self.client.post(self.api_url, self.nom_object)
        self.assertEquals(response.status_code, 201)

    def test_username_field(self):
        # Assumptions: 
        # - Cannot be blank
        # - Can only contain characters a-z, A-Z, 0-9 and @.+-_
        # - Cannot be over 150 characters long

        field = "username"
        boundary_values = {
            "min-": [""],
            "min": ["a", "1", "@", ".", "+", "-", "_"],
            "nom": ["a1@.+-_"],
            "max": ["a" * 150],
            "max+": ["a" * 151, "?", "!", "£££"]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)

    def test_email_field(self):    
        field = "email"
        boundary_values = {
            "min-": [
                "test", 
                "@test.com", 
                "test@", 
                "test@test", 
                "testtest.com", 
                "test@.com", 
                ".@test.com", 
                ".t@test.com", 
                "t.@test.com", 
                "test@.", 
                "test@test.", 
                "test@test.c"
            ],
            "min": [
                "", 
                "t@t.co"
            ],
            "nom": [
                "TeSt1234567890@TeSt1234567890.c0M", 
                "-!#$%&'*+/=?^_`{}|~@test.com", 
                "test.test@test.com"
            ],
            "max": [
                "a" * (255 - 9 - 1) + "@test.com", 
                "test@" + ("a" * 63) + ".com",
                "test@test" + "." + ("a" * 63)
            ],
            "max+": [
                "a" * (255 - 9) + "@test.com", 
                "test@" + ("a" * 64) + ".com", 
                "test@test" + "." + ("a" * 64),
                "test@test@test.com", 
                "test@@test.com", 
                "test@test..com", 
                "test.@test.com", 
                ".test@test.com", 
                "test..test@test.com", 
                "test£@test.com", 
                "test@test!.com",  
                "test@test.co!m"
            ]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)


    def test_password_field(self):
        # Assumptions:
        # - Cannot be blank
        # - Can be infinitely long
        # - Can contain any charachter

        field = "password"
        boundary_values = {
            "min-": [""],
            "min": ["a"],
            "nom": [
                "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
                "a" * 1000
            ]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)

    def test_password1_field(self):
        # Assumptions:
        # - Cannot be blank
        # - Can be infinitely long
        # - Can contain any charachter

        field = "password1"
        boundary_values = {
            "min-": [""],
            "min": ["a"],
            "nom": [
                "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
                "a" * 1000
            ]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)

    def test_phone_number_field(self):
        # Assumptions:
        # - Can only be norwegian phone numbers without country codes (i.e. length of 8)
        # - Can only contain numbers 0-9
        # - Can be blank

        field = "phone_number"
        boundary_values = {
            "min-": ["9999999"],
            "min": ["", "00000000"],
            "nom": [
                "01234567",
                "23456789"
            ],
            "max": ["99999999"],
            "max+": ["100000000", "abc"]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)

    def test_country_field(self):
        # Assumptions:
        # - Can only contain characters a-z, A-Z, - and æøåÆØÅ
        # - Cannot start or end with "-"
        # - Must be at least 2 characters long
        # - Cannot be over 50 characters long
        # - Cannot be "--"
        # - Can be blank

        field = "country"
        boundary_values = {
            "min-": ["a"],
            "min": ["", "aa"],
            "nom": ["abcdefghijklmnopqrstuvwxyzæøå", "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ", "test-test"],
            "max": ["a" * 50],
            "max+": ["a" * 51, "!\"#$%&\'()*+,./0123456789:;<=>?@[\\]^_`{|}~", "test-", "-test"]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)

    def test_city_field(self):
        # Assumptions:
        # - Can only contain characters a-z, A-Z, -, 0-9 and æøåÆØÅ
        # - Cannot start or end with "-"
        # - Must be at least 2 characters long
        # - Cannot be over 50 characters long
        # - Cannot be "--"
        # - Can be blank

        field = "city"
        boundary_values = {
            "min-": ["a"],
            "min": ["", "aa"],
            "nom": ["abcdefghijklmnopqrstuvwxyzæøå", "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ", "test-test"],
            "max": ["a" * 50],
            "max+": ["a" * 51, "!\"#$%&\'()*+,./0123456789:;<=>?@[\\]^_`{|}~", "test-", "-test"]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)

    def test_street_address_field(self):
        # Assumptions:
        # - Can only contain characters a-z, A-Z, -, 0-9 and æøåÆØÅ
        # - Cannot start or end with "-"
        # - Must be at least 2 characters long
        # - Cannot be over 50 characters long
        # - Cannot be "--"
        # - Can be blank

        field = "street_address"
        boundary_values = {
            "min-": ["a"],
            "min": ["", "aa"],
            "nom": ["abcdefghijklmnopqrstuvwxyzæøå-123456789", "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ-123456789"],
            "max": ["a" * 50],
            "max+": ["a" * 51, "!\"#$%&\'()*+,./0123456789:;<=>?@[\\]^_`{|}~", "-test", "test-"]
        }

        self.assertBoundaryValues(field, boundary_values, flush=True)