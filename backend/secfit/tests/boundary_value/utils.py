from django.core import management
from django.test import TestCase

class ExtendedTestCase(TestCase):
    nom_object = {}
    api_url = ""

    def assertBoundaryValues(self, field, boundary_values, flush=False):
        failures = []

        for key, values in boundary_values.items():          
            for value in values:
                if flush:
                    management.call_command('flush', '--noinput') # Clear database

                test_values = self.nom_object.copy()
                test_values[field] = value
            
                response = self.client.post(self.api_url, test_values)


                if key in ["min-", "max+"]:
                    # Should return status code of 400 (not able to create object)
                    if (response.status_code != 400):
                        failures.append(f"failed assertion for {field} {key}: {value}")
                else:
                    # Should return status code of 201 (able to create object)
                    if (response.status_code != 201):
                        failures.append(f"failed assertion for {field} {key}: {value}")      

        for failure in failures:
            print(failure)
        
        self.assertEquals(len(failures), 0)