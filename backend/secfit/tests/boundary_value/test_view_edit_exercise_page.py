"""
Boundary value tests for the view/edit exercise page.
"""

import math
from tests.boundary_value.utils import ExtendedTestCase

from rest_framework.test import APIClient
from django.contrib.auth import get_user_model


class ViewEditExercisePageTest(ExtendedTestCase):

    def setUp(self):
        self.client = APIClient()
        
        self.api_url = "/api/exercises/"

        self.user = get_user_model().objects.create_user(username="test", password="test")
        self.client.force_authenticate(user=self.user)

        self.nom_object = {
            "name": "Push-ups",
            "description": "Push yourself upwards with your arms.",
            "unit": "reps",
            "duration": "10",
            "calories": "100",            
            "muscleGroup": "Arms"
        }

    def test_nom_object(self):
        response = self.client.post(self.api_url, self.nom_object)
        self.assertEquals(response.status_code, 201)

    def test_unit_field(self):
        # Assumptions:
        # - Can only contain characters a-z, A-Z, - and æøåÆØÅ
        # - Cannot start or end with "-"
        # - Cannot be over 50 characters long
        # - Cannot be blank

        field = "unit"
        boundary_values = {
            "min-": [""],
            "min": ["s"],
            "nom": ["abcdefghijklmnopqrstuvwxyzæøå", "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ", "test-test"],
            "max": ["a" * 50],
            "max+": ["a" * 51, "!\"#$%&\'()*+,./0123456789:;<=>?@[\\]^_`{|}~", "-reps", "reps-"]
        }

        self.assertBoundaryValues(field, boundary_values)

    def test_duration_field(self):
        # Assumptions:
        # - Can only contain characters 0-9
        # - Must be 1 or more
        # - Cannot be blank

        field = "duration"
        boundary_values = {
            "min-": ["", "0"],
            "min": ["1"],
            "nom": ["5", "100"],
            "max+": ["a", "-"]
        }

        self.assertBoundaryValues(field, boundary_values)

    def test_calories_field(self):
        # Assumptions:
        # - Can only contain characters 0-9
        # - Must be 1 or more
        # - Cannot be blank

        field = "calories"
        boundary_values = {
            "min-": ["", "0"],
            "min": ["1"],
            "nom": ["5", "100"],
            "max+": ["a", "-"]
        }

        self.assertBoundaryValues(field, boundary_values)