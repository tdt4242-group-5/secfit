"""
Tests for the workout permissions.
"""

from django.test import TestCase, RequestFactory
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly
from users.models import User
from workouts.models import Workout, Exercise, ExerciseInstance

class TestIsOwner(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="mock")
        self.request_factory = RequestFactory()
    
    def test_has_object_permission(self):
        req = self.request_factory.get("/")
        req.user = self.user
        workout = Workout.objects.create(name="mocked_workout", date="2022-03-15T16:59:00Z", notes="", owner=self.user, visibility="PU")
        self.assertTrue(IsOwner().has_object_permission(req, None, workout))

class TestIsOwnerOfWorkout(TestCase):

    def setUp(self):
        self.request_factory = RequestFactory()
        self.user = User.objects.create(username="mock")
    
    def test_has_permission(self):
        request = self.request_factory.post("/")
        request.user = self.user
        request.data = {"workout": "/api/workouts/1/"}
        Workout.objects.create(name="mocked_workout", date="2022-03-15T16:59:00Z", notes="", owner=self.user, visibility="PU")
        self.assertTrue(IsOwnerOfWorkout().has_permission(request, None))

    def test_no_workout_has_permissions(self):
        request = self.request_factory.post("/")
        request.user = self.user
        request.data = {}
        self.assertFalse(IsOwnerOfWorkout().has_permission(request, None))
    
    def test_not_post_has_permissions(self):
        request = self.request_factory.get("/")
        self.assertTrue(IsOwnerOfWorkout().has_permission(request, None))

    def test_has_object_permissions(self):
        request = self.request_factory.get("/")
        request.user = self.user
        workout = Workout.objects.create(name="mocked_workout", date="2022-03-15T16:59:00Z", notes="", owner=self.user, visibility="PU")
        exercise = Exercise.objects.create(name="mocked_exercise", description="", unit="Times", owner=self.user)
        exercise_instance = ExerciseInstance.objects.create(workout=workout, exercise=exercise, sets=3, number=12)

        self.assertTrue(IsOwnerOfWorkout().has_object_permission(request, None, exercise_instance))

class TestIsCoachAndVisibleToCoach(TestCase):

    def test_has_object_permissions(self):
        coach = User.objects.create(username="mocked_coach")
        object_owner = User.objects.create(username="mocked_user")
        object_owner.coach = coach

        request = RequestFactory().get("/")
        request.user = coach

        workout = Workout.objects.create(name="mocked_workout", date="2022-03-15T16:59:00Z", notes="", owner=object_owner, visibility="CO")
        self.assertTrue(IsCoachAndVisibleToCoach().has_object_permission(request, None, workout))

class TestIsCoachOfWorkoutAndVisibleToCoach(TestCase):

     def test_has_object_permissions(self):
        coach = User.objects.create(username="mocked_coach")
        object_owner = User.objects.create(username="mocked_user")
        object_owner.coach = coach

        request = RequestFactory().get("/")
        request.user = coach

        workout = Workout.objects.create(name="mocked_workout", date="2022-03-15T16:59:00Z", notes="", owner=object_owner, visibility="CO")
        exercise = Exercise.objects.create(name="mocked_exercise", description="", unit="Times", owner=object_owner)
        exercise_instance = ExerciseInstance.objects.create(workout=workout, exercise=exercise, sets=3, number=12)
        self.assertTrue(IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(request, None, exercise_instance))

class TestIsPublic(TestCase):

    def test_has_object_permissions(self):
        owner = User.objects.create(username="mocked_user")
        request = RequestFactory().get("/")
        request.user = owner

        workout = Workout.objects.create(name="mocked_workout", date="2022-03-15T16:59:00Z", notes="", owner=owner, visibility="PU")
        self.assertTrue(IsPublic().has_object_permission(request, None, workout))

class TestIsWorkoutPublic(TestCase):

    def test_has_object_permissions(self):
        owner = User.objects.create(username="mocked_user")
        request = RequestFactory().get("/")
        request.user = owner

        workout = Workout.objects.create(name="mocked_workout", date="2022-03-15T16:59:00Z", notes="", owner=owner, visibility="PU")
        exercise = Exercise.objects.create(name="mocked_exercise", description="", unit="Times", owner=owner)
        exercise_instance = ExerciseInstance.objects.create(workout=workout, exercise=exercise, sets=3, number=12)
        self.assertTrue(IsWorkoutPublic().has_object_permission(request, None, exercise_instance))

class TestIsReadOnly(TestCase):

    def test_has_object_permissions(self):
        user = User.objects.create(username="mocked_user")
        get_request = RequestFactory().get("/")
        get_request.user = user
        head_request = RequestFactory().head("/")
        head_request.user = user
        options_request = RequestFactory().options("/")
        options_request.user = user

        self.assertTrue(IsReadOnly().has_object_permission(get_request, None, None))
        self.assertTrue(IsReadOnly().has_object_permission(head_request, None, None))
        self.assertTrue(IsReadOnly().has_object_permission(options_request, None, None))