"""
Tests for the user serializer.
"""

from django.test import TestCase
from rest_framework.test import APIRequestFactory
from rest_framework.request import Request

from users.models import User
from users.serializers import UserSerializer

class UserSerializerTests(TestCase):

    def setUp(self):
        
        # Mock the user url field
        request_factory = APIRequestFactory()
        request = request_factory.post("/")
        serializer_context = {
            "request": Request(request)
        }

         # Data to mock a user 
        self.user_attributes = {
            "username": "random",
            "email": "admin@admin.com",
            "password": "123",
            "phone_number": "12345678",
            "country": "norway",
            "city": "trondheim",
            "street_address": "road 1A"
        }

        # Making the user object and serializer available for the test class
        self.user = User.objects.create(**self.user_attributes)
        self.serializer = UserSerializer(context=serializer_context)

    def test_user_serialization(self):
        attr = self.user_attributes
        attr["username"] = "mock"
        user_object = self.serializer.create(self.user_attributes)
        self.assertEqual(user_object.username, "mock")
