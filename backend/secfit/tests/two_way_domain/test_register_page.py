from itertools import product
from django.test import TestCase, Client
from django.core import management

class UserRegistration(TestCase):

    def test_two_way_domain(self):
        self.client = Client()     
        
        fields = ["username", "email", "password", "password1", "phone_number", "country", "city", "street_address"]
        
        invalids = {
            "username": ":=)", 
            "email": "@@@", 
            "password": "", 
            "password1": "",
            "phone_number": "1234567", 
            "country": "123", 
            "city": "123", 
            "street_address": "123"
        }
       
        fail_count = 0
        successful_posts = []

        default_payload = {
            # a default payload that gets pairs of wrong values injected
                "username": "mock", 
                "email": "mock@mock.com", 
                "password": "mock123", 
                "password1": "mock123",
                "phone_number": "12345678", 
                "country": "Norway", 
                "city": "Trondheim", 
                "street_address": "road 1A"
            }

        tested_combinations = []
        
        all_combinations = set() 
        temp = [all_combinations.add((a, b)) for (a, b) in list(product(fields, fields))
              if (a, b) and (b, a) not in all_combinations]

        for keys in list(all_combinations):
            if(keys[0] == keys[1]): continue
            payload = default_payload.copy()
            for key in keys:
                payload[key] = invalids[key]
            response = self.client.post("/api/users/", payload)
            try:
                self.assertEqual(response.status_code, 201)
                successful_post = {"payload_invalid_keys": keys, "response": response.status_code, "payload": payload}
                successful_posts.append(successful_post)
            except AssertionError as e:
                fail_count += 1
            management.call_command('flush', '--noinput') # Clear database so that equal users can be created
            tested_combinations.append(keys)
        
        print(successful_posts) # Report of payloads that should fail, but doesn´t
        self.assertEquals(fail_count, len(all_combinations)-8) # -8 for the equal keysets
            

