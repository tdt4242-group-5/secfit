from rest_framework import serializers
from workouts.models import Workout, Exercise
from meals.models import Meal
from workouts.serializers import ExerciseSerializer, WorkoutSerializer
from meals.serializers import MealSerializer
from notifications.models import Notification
from generic_relations.relations import GenericRelatedField

class NotificationSerializer(serializers.HyperlinkedModelSerializer):
    content_object = GenericRelatedField({
        Workout: WorkoutSerializer(),
        Exercise: ExerciseSerializer(),
        Meal: MealSerializer()
    })
    content_type = serializers.SerializerMethodField('get_content_type', read_only=True)

    class Meta:
        model = Notification
        fields = ['timestamp', 'read', 'content_type', 'object_id', 'content_object']

    def get_content_type(self, notification):
        return notification.content_type.name