from django.urls import path, include
from notifications import views
from rest_framework.urlpatterns import format_suffix_patterns

# This is messy and should be refactored
urlpatterns = format_suffix_patterns(
    [
        path("", views.api_root),
        path("api/notifications/", views.NotificationList.as_view(), name="notification-list"),
        path("api/notifications/read", views.NotificationReadView.as_view(), name="notification-list-read"),
        path("api/notifications/unread/count/", views.NotificationUnreadCountView.as_view(), name="notification-list-unread-count"),
        path("", include("users.urls")),
    ]
)
