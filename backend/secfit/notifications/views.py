from django.dispatch import receiver
from rest_framework import generics, mixins
from rest_framework import permissions

from notifications.serializers import NotificationSerializer
from .models import Notification
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "users": reverse("user-list", request=request, format=format),
            "notifications": reverse("notification-list", request=request, format=format)
        }
    )

# Create your views here.

class NotificationList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of a notification, or displaying a list
    of notifications

    HTTP methods: GET, POST
    """

    serializer_class = NotificationSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]  # User must be authenticated to create/view notifications

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)

    def get_queryset(self):
        qs = Notification.objects.none()

        if self.request.user:
            qs = Notification.objects.filter(receiver=self.request.user).distinct()
        
        return qs


class NotificationReadView(APIView):
    def put(self, request, *args, **kwargs):
        if self.request.user:
            Notification.objects.filter(receiver=self.request.user).update(read=True)

        return Response()


class NotificationUnreadCountView(generics.GenericAPIView):
    """
    A view that returns the count of unread notifications for a user.
    """

    permission_classes = [
        permissions.IsAuthenticated
    ]  # User must be authenticated to view notification count

    def get(self, request, *args, **kwargs):
        if self.request.user:
            return Response(Notification.objects.filter(Q(receiver=self.request.user) & Q(read=False)).distinct().count())
        return 0
