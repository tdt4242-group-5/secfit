from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.utils import timezone

# Create your models here.

class Notification(models.Model):
    """Django model for a notification that users get when their followers post a new workout, exercise or meal.

    A notification has several attributes, and is associated with one sender (poster), one receiver (follower) and a post (workout, exercise or meal). A notification is marked as opened when the receiver has read the notification.

    Attributes:
        timestamp:      Date and time the notification was created (and the post was created)
        conent_type:    Type of content (workout, exercise or meal) the notification is for    
        object_id:      ID of the post the notifcation is for
        content_object: Content of the post the notification is for
        receiver:       The receiver of the notification
        read:           If the receiver has read the notification or not
    """

    timestamp = models.DateTimeField(default=timezone.now)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    receiver = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="notifications"
    )
    read = models.BooleanField(default=False)

    class Meta:
        ordering = ["-timestamp"]

